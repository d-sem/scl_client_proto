<?php

$test_data = generateRandomString(200000);
$refresh_token = file_get_contents('.refresh_token');
$auth_token =  file_get_contents('.auth_token');
$server_public_key = file_get_contents('.s_key.pub');
$cipher = "aes-256-cbc";
$url = "http://5.187.1.80:3000/echo";

$key = openssl_random_pseudo_bytes(32);
$ivlen = openssl_cipher_iv_length($cipher);
$iv = openssl_random_pseudo_bytes($ivlen);
$encrypted_data = openssl_encrypt($test_data, $cipher, $key, $option = 0, $iv);

$data = [];
$data['key'] = base64_encode($key);
$data['iv'] = base64_encode($iv);
$data = base64_encode(json_encode($data));

openssl_public_encrypt($data, $encrypted_key, $server_public_key);
$encrypted_key = base64_encode($encrypted_key);

$query = array(
    'auth_token' => $auth_token,
    'key' => $encrypted_key,
    'encrypted' => $encrypted_data,
    'refresh_token' => $refresh_token,
);

$ch = curl_init($url);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$result = curl_exec($ch);

var_dump($result == $test_data);

function generateRandomString($length = 1000) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}