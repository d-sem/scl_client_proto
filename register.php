<?php

$reg_code = '2ck7pu';
$url = 'http://5.187.1.80:3000/reg';

$config = array(
    "digest_alg" => "sha512",
    "private_key_bits" => 4096,
    "private_key_type" => OPENSSL_KEYTYPE_RSA,
);
$res = openssl_pkey_new($config);
openssl_pkey_export($res, $private_key);

$public_key = openssl_pkey_get_details($res)["key"];

$ch = curl_init($url);
$query = array(
    'code' => $reg_code,
    'public_key' => $public_key,
);

curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$result = json_decode(curl_exec($ch));

file_put_contents('.s_key.pub', base64_decode($result->public));
file_put_contents('.auth_token', base64_decode($result->auth_token));
file_put_contents('.refresh_token', base64_decode($result->refresh_token));
file_put_contents('.c_key.priv', $private_key);
file_put_contents('.c_key.pub', $public_key);