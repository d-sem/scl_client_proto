<?php

$auth_token =  file_get_contents('.auth_token');
$client_private_key = file_get_contents('.c_key.priv');
$refresh_token = file_get_contents('.refresh_token');

$test_data = generateRandomString(10);
$url = "http://5.187.1.80:3000/echo";
$cipher = "aes-256-cbc";

$query = http_build_query([
    'auth_token' => $auth_token,
    'data' => $test_data,
    'refresh_token' => $refresh_token,
]);

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url . '?' . $query );
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$result = json_decode(curl_exec($ch));

$encrypted = $result->encrypted;
$aes_key = base64_decode($result->key);
openssl_private_decrypt ($aes_key, $aes_key , $client_private_key, OPENSSL_PKCS1_OAEP_PADDING);
$key = json_decode($aes_key);
$aes_key = base64_decode($key->key);
$aes_iv = base64_decode($key->iv);

$decrypted = openssl_decrypt($encrypted, $cipher, $aes_key, $option = 0, $aes_iv);

var_dump($decrypted == $test_data);

function generateRandomString($length = 10000) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}